# guild-user
Reads and stores user data for a guild.

Port range 8100-8199

Requires [https://gitlab.com/discord-rpg-bot/model](https://gitlab.com/discord-rpg-bot/model) and name server to be running on port 9000

Also needs a mongodb database "guildUserDB" running on port 27017

Runtime documentation [http://localhost:8100/swagger-ui/index.html](http://localhost:8100/swagger-ui/index.html#/)