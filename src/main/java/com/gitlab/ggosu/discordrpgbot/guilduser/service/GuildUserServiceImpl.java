package com.gitlab.ggosu.discordrpgbot.guilduser.service;

import com.gitlab.ggosu.discordrpgbot.guilduser.api.GuildUserService;
import com.gitlab.ggosu.discordrpgbot.model.user.UserBuff;
import com.gitlab.ggosu.discordrpgbot.model.user.UserItem;
import com.gitlab.ggosu.discordrpgbot.model.user.guild.GuildUser;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@AllArgsConstructor
@Service
public class GuildUserServiceImpl implements GuildUserService {
    private final GuildUserRepository guildUserRepository;

    // guild user ----------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addGuildUser(GuildUser guildUser) {
        guildUserRepository.insert(guildUser);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<List<GuildUser>> getAllGuildUsers() {
        List<GuildUser> allGuildUsers = guildUserRepository.findAll();
        if(allGuildUsers.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(allGuildUsers);
    }

    @Override
    public ResponseEntity<Optional<GuildUser>> getGuildUserById(String id) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(guildUser);
    }

    @Override
    public ResponseEntity<Object> deleteGuildUserById(String id) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        guildUserRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // character -----------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addCharacterToGuildUser(String id, Byte characterId) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        if(guildUser.get().getCharacterIds().contains(characterId)){
            //return ResponseEntity.status(HttpStatus.CONFLICT).build();
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }

        guildUser.get().getCharacterIds().add(characterId);
        guildUser.get().setCharacterActiveId(characterId);
        guildUserRepository.save(guildUser.get());

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Object> deleteCharacterFromGuildUser(String id, Byte characterId) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        if(!guildUser.get().getCharacterIds().contains(characterId) && !guildUser.get().getCharacterActiveId().equals(characterId)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        guildUser.get().getCharacterIds().remove(characterId);
        if(Objects.equals(guildUser.get().getCharacterActiveId(), characterId)){
            if(guildUser.get().getCharacterIds().size() > 0){
                guildUser.get().setCharacterActiveId(guildUser.get().getCharacterIds().get(0));
            }
            else{
                guildUser.get().setCharacterActiveId(null);
            }
        }

        guildUserRepository.save(guildUser.get());
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Override
    public ResponseEntity<Object> setActiveCharacterForGuildUser(String id, Byte characterId) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        if(!guildUser.get().getCharacterIds().contains(characterId)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        guildUser.get().setCharacterActiveId(characterId);
        guildUserRepository.save(guildUser.get());

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // items ---------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addItemToGuildUser(String id, Integer itemId, Integer amount) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserItem item : guildUser.get().getItems()){
            if(Objects.equals(item.getId(), itemId)){
                item.setAmount(item.getAmount() + amount);
                guildUserRepository.save(guildUser.get());
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
        }

        guildUser.get().getItems().add(new UserItem(itemId, amount));
        guildUserRepository.save(guildUser.get());

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Object> removeItemFromGuildUserByAmount(String id, Integer itemId, Integer amount) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserItem item : guildUser.get().getItems()){
            if(Objects.equals(item.getId(), itemId)){
                if(item.getAmount() - amount < 0){
                    return ResponseEntity.status(HttpStatus.CONFLICT).build();
                }
                if(item.getAmount() - amount == 0){
                    guildUser.get().getItems().remove(item);
                    guildUserRepository.save(guildUser.get());
                    return ResponseEntity.status(HttpStatus.OK).build();
                }
                item.setAmount(item.getAmount() - amount);
                guildUserRepository.save(guildUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @Override
    public ResponseEntity<Object> deleteItemFromGuildUserById(String id, Integer itemId) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserItem item : guildUser.get().getItems()){
            if(Objects.equals(item.getId(), itemId)){
                guildUser.get().getItems().remove(item);
                guildUserRepository.save(guildUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // buffs ---------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addBuffToGuildUser(String id, Integer buffId) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserBuff buff : guildUser.get().getBuffs()){
            if(Objects.equals(buff.getId(), buffId)){
                buff.setTimestamp(new Date());
                guildUserRepository.save(guildUser.get());
                return ResponseEntity.status(HttpStatus.CREATED).build();
            }
        }

        guildUser.get().getBuffs().add(new UserBuff(buffId, new Date()));
        guildUserRepository.save(guildUser.get());

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override
    public ResponseEntity<Object> deleteBuffFromGuildUser(String id, Integer buffId) {
        Optional<GuildUser> guildUser = guildUserRepository.findById(id);
        if(guildUser.isEmpty()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        for(UserBuff buff : guildUser.get().getBuffs()){
            if(Objects.equals(buff.getId(), buffId)){
                guildUser.get().getBuffs().remove(buff);
                guildUserRepository.save(guildUser.get());
                return ResponseEntity.status(HttpStatus.OK).build();
            }
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
