package com.gitlab.ggosu.discordrpgbot.guilduser.api;

import com.gitlab.ggosu.discordrpgbot.model.user.guild.GuildUser;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public interface GuildUserService {
    // guild user
    /**
     * Adds a guild user to the database
     */
    @PostMapping("/user/guild")
    ResponseEntity<Object> addGuildUser(@RequestBody GuildUser guildUser);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Gets all guild users
     */
    @GetMapping("/user/guild")
    ResponseEntity<List<GuildUser>> getAllGuildUsers();
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Gets a guild user by id
     */
    @GetMapping("/user/guild/{id}")
    ResponseEntity<Optional<GuildUser>> getGuildUserById(@PathVariable String id);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes a guild user by id
     */
    @DeleteMapping("/user/guild/{id}")
    ResponseEntity<Object> deleteGuildUserById(@PathVariable String id);
    // ---------------------------------------------------------------------------------------------------------------------

    // character
    /**
     * Adds new character index to a guild user
     */
    @PostMapping("/user/guild/{id}/character/{characterId}")
    ResponseEntity<Object> addCharacterToGuildUser(@PathVariable String id, @PathVariable Byte characterId);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes a character index from a guild user
     */
    @DeleteMapping("/user/guild/{id}/character/{characterId}")
    ResponseEntity<Object> deleteCharacterFromGuildUser(@PathVariable String id, @PathVariable Byte characterId);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Sets a guild user's active character index
     */
    @PostMapping("/user/guild/{id}/character/{characterId}/active")
    ResponseEntity<Object> setActiveCharacterForGuildUser(@PathVariable String id, @PathVariable Byte characterId);
    // ---------------------------------------------------------------------------------------------------------------------

    // items
    /**
     * Adds new item to a guild user
     */
    @PostMapping("/user/guild/{id}/items/{itemId}/{amount}")
    ResponseEntity<Object> addItemToGuildUser(@PathVariable String id, @PathVariable Integer itemId, @PathVariable Integer amount);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Removes an item from a guild user by id and amount
     */
    @DeleteMapping("/user/guild/{id}/items/{itemId}/{amount}")
    ResponseEntity<Object> removeItemFromGuildUserByAmount(@PathVariable String id, @PathVariable Integer itemId, @PathVariable Integer amount);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Deletes an item from a guild user by id
     */
    @DeleteMapping("/user/guild/{id}/items/{itemId}")
    ResponseEntity<Object> deleteItemFromGuildUserById(@PathVariable String id, @PathVariable Integer itemId);
    // ---------------------------------------------------------------------------------------------------------------------

    // buffs
    /**
     * Adds new buff to a guild user
     */
    @PostMapping("/user/guild/{id}/buffs/{buffId}")
    ResponseEntity<Object> addBuffToGuildUser(@PathVariable String id, @PathVariable Integer buffId);
    // ---------------------------------------------------------------------------------------------------------------------

    /**
     * Removes a buff from a guild user by id
     */
    @DeleteMapping("/user/guild/{id}/buffs/{buffId}")
    ResponseEntity<Object> deleteBuffFromGuildUser(@PathVariable String id, @PathVariable Integer buffId);
}
