package com.gitlab.ggosu.discordrpgbot.guilduser.service;

import com.gitlab.ggosu.discordrpgbot.guilduser.api.GuildUserService;
import com.gitlab.ggosu.discordrpgbot.model.user.guild.GuildUser;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class GuildUserController implements GuildUserService {
    private GuildUserServiceImpl guildUserService;

    // guild user ----------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addGuildUser(GuildUser guildUser) {
        return guildUserService.addGuildUser(guildUser);
    }

    @Override
    public ResponseEntity<List<GuildUser>> getAllGuildUsers() {
        return guildUserService.getAllGuildUsers();
    }

    @Override
    public ResponseEntity<Optional<GuildUser>> getGuildUserById(String id) {
        return guildUserService.getGuildUserById(id);
    }

    @Override
    public ResponseEntity<Object> deleteGuildUserById(String id) {
        return guildUserService.deleteGuildUserById(id);
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // character -----------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addCharacterToGuildUser(String id, Byte characterId) {
        return guildUserService.addCharacterToGuildUser(id, characterId);
    }

    @Override
    public ResponseEntity<Object> deleteCharacterFromGuildUser(String id, Byte characterId) {
        return guildUserService.deleteCharacterFromGuildUser(id, characterId);
    }

    @Override
    public ResponseEntity<Object> setActiveCharacterForGuildUser(String id, Byte characterId) {
        return guildUserService.setActiveCharacterForGuildUser(id, characterId);
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // items ---------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addItemToGuildUser(String id, Integer itemId, Integer amount) {
        return guildUserService.addItemToGuildUser(id, itemId, amount);
    }

    @Override
    public ResponseEntity<Object> removeItemFromGuildUserByAmount(String id, Integer itemId, Integer amount) {
        return guildUserService.removeItemFromGuildUserByAmount(id, itemId, amount);
    }

    @Override
    public ResponseEntity<Object> deleteItemFromGuildUserById(String id, Integer itemId) {
        return guildUserService.deleteItemFromGuildUserById(id, itemId);
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // buffs ---------------------------------------------------------------------------------------------------------------
    @Override
    public ResponseEntity<Object> addBuffToGuildUser(String id, Integer buffId) {
        return guildUserService.addBuffToGuildUser(id, buffId);
    }

    @Override
    public ResponseEntity<Object> deleteBuffFromGuildUser(String id, Integer buffId) {
        return guildUserService.deleteBuffFromGuildUser(id, buffId);
    }

}
