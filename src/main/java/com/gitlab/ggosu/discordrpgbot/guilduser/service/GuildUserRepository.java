package com.gitlab.ggosu.discordrpgbot.guilduser.service;

import com.gitlab.ggosu.discordrpgbot.model.user.guild.GuildUser;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GuildUserRepository extends MongoRepository<GuildUser, String> {

}
